/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 23 2023 20:59:16
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <game/client/globals.hh>

util::CommandLine globals::cmdline = {};
GLFWwindow *globals::window = nullptr;
float globals::frametime = 0.0f;
float globals::frametime_avg = 0.0f;
uint64_t globals::curtime = 0;
uint64_t globals::framecount = 0;
