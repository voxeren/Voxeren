/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 01:02:07
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <GLFW/glfw3.h>
#include <game/client/globals.hh>
#include <game/client/screen.hh>
#include <spdlog/spdlog.h>

static float calc_aspect(int width, int height)
{
    if(width > height)
        return static_cast<float>(width) / static_cast<float>(height);
    return static_cast<float>(height) / static_cast<float>(width);
}

static void on_framebuffer_size(GLFWwindow *window, int width, int height)
{
    ScreenSizeEvent event = {};
    event.width = width;
    event.height = height;
    event.aspect = calc_aspect(width, height);
    globals::dispatcher.trigger(event);
}

void screen_system::init()
{
    spdlog::debug("screen: taking over framebuffer events");
    glfwSetFramebufferSizeCallback(globals::window, &on_framebuffer_size);
}

void screen_system::init_late()
{
    int width, height;
    glfwGetFramebufferSize(globals::window, &width, &height);
    on_framebuffer_size(globals::window, width, height);
}

int screen::width()
{
    int width;
    glfwGetFramebufferSize(globals::window, &width, nullptr);
    return width;
}

int screen::height()
{
    int height;
    glfwGetFramebufferSize(globals::window, nullptr, &height);
    return height;
}

void screen::size(int &width, int &height)
{
    glfwGetFramebufferSize(globals::window, &width, &height);
}

void screen::size(float &width, float &height)
{
    int iwidth, iheight;
    glfwGetFramebufferSize(globals::window, &iwidth, &iheight);
    width = static_cast<float>(iwidth);
    height = static_cast<float>(iheight);
}

float screen::aspect()
{
    int width, height;
    glfwGetFramebufferSize(globals::window, &width, &height);
    return calc_aspect(width, height);
}
