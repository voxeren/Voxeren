/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 00:51:46
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef C119F935_C7C5_413B_9FC7_0E9AF7EDC0C5
#define C119F935_C7C5_413B_9FC7_0E9AF7EDC0C5
#include <GLFW/glfw3.h>

struct CursorPosEvent final {
    float xpos;
    float ypos;
};

struct KeyEvent final {
    int key;
    int scancode;
    int action;
    int mods;
};

struct MouseButtonEvent final {
    int button;
    int action;
    int mods;
};

struct ScrollEvent final {
    float dx;
    float dy;
};

namespace input_system
{
void init();
} // namespace input_system

#endif /* C119F935_C7C5_413B_9FC7_0E9AF7EDC0C5 */
