/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 00:54:09
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <game/client/globals.hh>
#include <game/client/input.hh>
#include <spdlog/spdlog.h>

static void on_cursor_pos(GLFWwindow *window, double xpos, double ypos)
{
    CursorPosEvent event = {};
    event.xpos = static_cast<float>(xpos);
    event.ypos = static_cast<float>(ypos);
    globals::dispatcher.trigger(event);
}

static void on_key(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    KeyEvent event = {};
    event.key = key;
    event.scancode = scancode;
    event.action = action;
    event.mods = mods;
    globals::dispatcher.trigger(event);
}

static void on_mouse_button(GLFWwindow *window, int button, int action, int mods)
{
    MouseButtonEvent event = {};
    event.button = button;
    event.action = action;
    event.mods = mods;
    globals::dispatcher.trigger(event);
}

static void on_scroll(GLFWwindow *window, double dx, double dy)
{
    ScrollEvent event = {};
    event.dx = static_cast<float>(dx);
    event.dy = static_cast<float>(dy);
    globals::dispatcher.trigger(event);
}

void input_system::init()
{
    spdlog::debug("input: taking over input events");
    glfwSetCursorPosCallback(globals::window, &on_cursor_pos);
    glfwSetKeyCallback(globals::window, &on_key);
    glfwSetMouseButtonCallback(globals::window, &on_mouse_button);
    glfwSetScrollCallback(globals::window, &on_scroll);
}
