/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 23 2023 13:44:06
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef DB9DD8FD_302B_4AF3_916C_9975EDAF3064
#define DB9DD8FD_302B_4AF3_916C_9975EDAF3064
#include <util/cmdline.hh>

namespace client
{
void main(const util::CommandLine &cmdline);
} // namespace client

#endif /* DB9DD8FD_302B_4AF3_916C_9975EDAF3064 */
