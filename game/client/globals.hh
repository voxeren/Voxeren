/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 23 2023 20:55:28
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef EDB83790_D19E_4070_ACE4_B9CAD78B1023
#define EDB83790_D19E_4070_ACE4_B9CAD78B1023
#include <game/shared/globals.hh>
#include <GLFW/glfw3.h>
#include <util/cmdline.hh>

namespace globals
{
extern util::CommandLine cmdline;
extern GLFWwindow *window;
extern float frametime;
extern float frametime_avg;
extern uint64_t curtime;
extern uint64_t framecount;
} // namespace globals

#endif /* EDB83790_D19E_4070_ACE4_B9CAD78B1023 */
