/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 23 2023 21:10:20
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef D63BB724_F901_4F5D_BB28_52BBC71CD633
#define D63BB724_F901_4F5D_BB28_52BBC71CD633

namespace client_game
{
void init();
void init_late();
void deinit();
void update();
void update_late();
void render();
} // namespace client_game

#endif /* D63BB724_F901_4F5D_BB28_52BBC71CD633 */
