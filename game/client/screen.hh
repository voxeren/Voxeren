/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 01:00:18
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef EA52E989_F5ED_4B95_BDE7_CF94B6C9B0D9
#define EA52E989_F5ED_4B95_BDE7_CF94B6C9B0D9

struct ScreenSizeEvent final {
    int width;
    int height;
    float aspect;
};

namespace screen_system
{
void init();
void init_late();
} // namespace screen_system

namespace screen
{
int width();
int height();
void size(int &width, int &height);
void size(float &width, float &height);
float aspect();
} // namespace screen

#endif /* EA52E989_F5ED_4B95_BDE7_CF94B6C9B0D9 */
