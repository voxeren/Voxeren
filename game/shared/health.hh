/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 14:49:57
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef F8C73353_C23B_4B8F_A8DE_FE378BF846AB
#define F8C73353_C23B_4B8F_A8DE_FE378BF846AB
#include <stdint.h>

struct HealthComponent final {
    // An entity with negative health values is
    // set to a dying state, dropping items, playing
    // a death animation and being purged from the registry.
    // Entities without health component cannot be hurt...
    int32_t health;
};

#endif /* F8C73353_C23B_4B8F_A8DE_FE378BF846AB */
