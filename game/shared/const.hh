/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 14:17:03
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef BC6429F4_60D8_4913_86E9_92F25A1C3031
#define BC6429F4_60D8_4913_86E9_92F25A1C3031
#include <math/common.hh>

constexpr static const size_t CHUNK_SIZE = 16;
constexpr static const size_t CHUNK_SLICE = CHUNK_SIZE * CHUNK_SIZE;
constexpr static const size_t CHUNK_VOLUME = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;
constexpr static const size_t CHUNK_SIZE_LOG2 = math::log2(CHUNK_SIZE);

#endif /* BC6429F4_60D8_4913_86E9_92F25A1C3031 */
