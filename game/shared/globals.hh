/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 23 2023 19:39:07
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef ED195013_40BC_4BC4_8886_79FBBF971CB2
#define ED195013_40BC_4BC4_8886_79FBBF971CB2
#include <entt/entity/registry.hpp>
#include <entt/signal/dispatcher.hpp>

namespace globals
{
extern entt::dispatcher dispatcher;
extern entt::registry registry;
} // namespace globals

#endif /* ED195013_40BC_4BC4_8886_79FBBF971CB2 */
