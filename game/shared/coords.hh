/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 14:25:13
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef A13D87AB_032F_4A09_8E0F_225B4D692AB0
#define A13D87AB_032F_4A09_8E0F_225B4D692AB0
#include <game/shared/const.hh>
#include <math/vector3.hh>

class ChunkCoord;
class LocalCoord;
class VoxelCoord;

class ChunkCoord final : public math::Packed<int32_t, 3> {
public:
    constexpr math::Vector3 to_world() const;
    constexpr VoxelCoord to_voxel(const LocalCoord &lvec) const;
};

class LocalCoord final : public math::Packed<int16_t, 3> {
public:
    constexpr size_t to_index() const;
    constexpr VoxelCoord to_voxel(const ChunkCoord &cvec) const;
};

class VoxelCoord final : public math::Packed<int64_t, 3> {
public:
    constexpr math::Vector3 to_world() const;
    constexpr ChunkCoord to_chunk() const;
    constexpr LocalCoord to_local() const;
};

class WorldCoord final {
public:
    static constexpr ChunkCoord to_chunk(const math::Vector3 &vec);
    static constexpr LocalCoord to_local(const math::Vector3 &vec);
    static constexpr VoxelCoord to_voxel(const math::Vector3 &vec);
};

class VoxelIndex final {
public:
    static constexpr LocalCoord to_local(size_t index);
};

inline constexpr math::Vector3 ChunkCoord::to_world() const
{
    return math::Vector3 {
        static_cast<float>(at(0) << CHUNK_SIZE_LOG2),
        static_cast<float>(at(1) << CHUNK_SIZE_LOG2),
        static_cast<float>(at(2) << CHUNK_SIZE_LOG2),
    };
}

inline constexpr VoxelCoord ChunkCoord::to_voxel(const LocalCoord &lvec) const
{
    return VoxelCoord {
        static_cast<VoxelCoord::value_type>(lvec[0] + (at(0) << CHUNK_SIZE_LOG2)),
        static_cast<VoxelCoord::value_type>(lvec[1] + (at(1) << CHUNK_SIZE_LOG2)),
        static_cast<VoxelCoord::value_type>(lvec[2] + (at(2) << CHUNK_SIZE_LOG2)),
    };
}

inline constexpr size_t LocalCoord::to_index() const
{
    return static_cast<size_t>((at(0) * CHUNK_SIZE + at(2)) * CHUNK_SIZE + at(1));
}

inline constexpr VoxelCoord LocalCoord::to_voxel(const ChunkCoord &cvec) const
{
    return VoxelCoord {
        static_cast<VoxelCoord::value_type>(at(0) + (cvec[0] << CHUNK_SIZE_LOG2)),
        static_cast<VoxelCoord::value_type>(at(1) + (cvec[1] << CHUNK_SIZE_LOG2)),
        static_cast<VoxelCoord::value_type>(at(2) + (cvec[2] << CHUNK_SIZE_LOG2)),
    };
}

inline constexpr math::Vector3 VoxelCoord::to_world() const
{
    return math::Vector3 {
        static_cast<float>(at(0)),
        static_cast<float>(at(1)),
        static_cast<float>(at(2)),
    };
}

inline constexpr ChunkCoord VoxelCoord::to_chunk() const
{
    return ChunkCoord {
        static_cast<ChunkCoord::value_type>(at(0) >> CHUNK_SIZE_LOG2),
        static_cast<ChunkCoord::value_type>(at(1) >> CHUNK_SIZE_LOG2),
        static_cast<ChunkCoord::value_type>(at(2) >> CHUNK_SIZE_LOG2),
    };
}

inline constexpr LocalCoord VoxelCoord::to_local() const
{
    return LocalCoord {
        static_cast<LocalCoord::value_type>(at(0) % CHUNK_SIZE),
        static_cast<LocalCoord::value_type>(at(1) % CHUNK_SIZE),
        static_cast<LocalCoord::value_type>(at(2) % CHUNK_SIZE),
    };
}

inline constexpr ChunkCoord WorldCoord::to_chunk(const math::Vector3 &vec)
{
    return ChunkCoord {
        static_cast<ChunkCoord::value_type>(math::floor<VoxelCoord::value_type>(vec.x) >> CHUNK_SIZE_LOG2),
        static_cast<ChunkCoord::value_type>(math::floor<VoxelCoord::value_type>(vec.y) >> CHUNK_SIZE_LOG2),
        static_cast<ChunkCoord::value_type>(math::floor<VoxelCoord::value_type>(vec.z) >> CHUNK_SIZE_LOG2),
    };
}

inline constexpr LocalCoord WorldCoord::to_local(const math::Vector3 &vec)
{
    return LocalCoord {
        static_cast<LocalCoord::value_type>(math::floor<VoxelCoord::value_type>(vec.x) % CHUNK_SIZE),
        static_cast<LocalCoord::value_type>(math::floor<VoxelCoord::value_type>(vec.y) % CHUNK_SIZE),
        static_cast<LocalCoord::value_type>(math::floor<VoxelCoord::value_type>(vec.z) % CHUNK_SIZE),
    };
}

inline constexpr VoxelCoord WorldCoord::to_voxel(const math::Vector3 &vec)
{
    return VoxelCoord {
        math::floor<VoxelCoord::value_type>(vec.x),
        math::floor<VoxelCoord::value_type>(vec.y),
        math::floor<VoxelCoord::value_type>(vec.z),
    };
}

inline constexpr LocalCoord VoxelIndex::to_local(size_t index)
{
    return LocalCoord {
        static_cast<LocalCoord::value_type>((index / CHUNK_SLICE)),
        static_cast<LocalCoord::value_type>((index % CHUNK_SIZE)),
        static_cast<LocalCoord::value_type>((index / CHUNK_SIZE) % CHUNK_SIZE),
    };
}

#endif /* A13D87AB_032F_4A09_8E0F_225B4D692AB0 */
