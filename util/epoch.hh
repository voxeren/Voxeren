#ifndef E411451D_CF13_4264_9EB3_224CB49C1288
#define E411451D_CF13_4264_9EB3_224CB49C1288
#include <stdint.h>

namespace util
{
uint64_t epoch_microseconds();
uint64_t epoch_milliseconds();
uint64_t epoch_seconds();
} // namespace util

#endif /* E411451D_CF13_4264_9EB3_224CB49C1288 */
