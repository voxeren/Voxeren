/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 23 2023 13:49:12
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <algorithm>
#include <ctype.h>
#include <util/cmdline.hh>

namespace detail
{
static inline bool is_valid_key(const std::string &argv)
{
    if(argv.find_last_of('-') >= argv.size() - 1)
        return false;
    return argv[0] == '-';
}

static inline const std::string get_key(const std::string &argv)
{
    size_t i;
    for(i = 0; argv[i] == '-'; i++);
    return std::string(argv.cbegin() + i, argv.cend());
}

// UNDONE: move this to strtools
static inline bool is_empty_or_whitespace(const std::string &s)
{
    if(s.empty())
        return true;
    return std::all_of(s.cbegin(), s.cend(), &isspace);
}
} // namespace detail

util::CommandLine::CommandLine(int argc, char **argv)
{
    append(argc, argv);
}

util::CommandLine::CommandLine(const CommandLine &other)
{
    append(other);
}

void util::CommandLine::append(int argc, char **argv)
{
    for(int i = 1; i < argc; ++i) {
        if(detail::is_valid_key(argv[i])) {
            const std::string key = detail::get_key(argv[i]);

            if(!detail::is_empty_or_whitespace(key)) {
                if(((i + 1) < argc) && !detail::is_valid_key(argv[i + 1])) {
                    map[key] = argv[++i];
                    set.erase(key);
                    continue;
                }

                map.erase(key);
                set.insert(key);
            }
        }
    }
}

void util::CommandLine::append(const std::string &key)
{
    map.erase(key);
    set.insert(key);
}

void util::CommandLine::append(const std::string &key, const std::string &value)
{
    map[key] = value;
    set.erase(key);
}

void util::CommandLine::append(const util::CommandLine &other)
{
    for(const auto it : other.map) {
        map[it.first] = it.second;
        set.erase(it.first);
    }

    for(const auto it : other.set) {
        map.erase(it);
        set.insert(it);
    }
}

bool util::CommandLine::contains(const std::string &key) const
{
    return set.count(key) || map.count(key);
}

bool util::CommandLine::has_value(const std::string &key) const
{
    return map.count(key);
}

bool util::CommandLine::get_value(const std::string &key, std::string &value) const
{
    const auto it = map.find(key);
    const auto jt = set.find(key);

    if(it != map.cend()) {
        value.assign(it->second);
        return true;
    }
    else if(jt != set.cend()) {
        value.clear();
        return true;
    }
    else {
        value.clear();
        return false;
    }
}
