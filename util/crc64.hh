/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Jul 15 2023 21:56:49
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef CA7BAA78_BD5A_4323_92A2_C88A860CC668
#define CA7BAA78_BD5A_4323_92A2_C88A860CC668
#include <stdint.h>
#include <string>
#include <vector>

namespace util
{
uint64_t crc64(const void *data, size_t size);
uint64_t crc64(const std::vector<uint8_t> &data);
uint64_t crc64(const std::string &data);
} // namespace util

#endif /* CA7BAA78_BD5A_4323_92A2_C88A860CC668 */
