/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 16 2023 16:34:51
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef AA59BB44_5791_4B8B_A07E_F7787647360E
#define AA59BB44_5791_4B8B_A07E_F7787647360E
#include <string>
#include <vector>

namespace util
{
std::vector<std::string> split(const std::string &value, const std::string &delim);
} // namespace util

#endif /* AA59BB44_5791_4B8B_A07E_F7787647360E */
