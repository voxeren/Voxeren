/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sun Jul 23 2023 13:45:07
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef C79D8ACB_84F0_4DDC_AAE8_86A47A6EF3D9
#define C79D8ACB_84F0_4DDC_AAE8_86A47A6EF3D9
#include <string>
#include <unordered_map>
#include <unordered_set>

namespace util
{
class CommandLine final {
public:
    CommandLine() = default;
    CommandLine(int argc, char **argv);
    CommandLine(const CommandLine &other);

    void append(int argc, char **argv);
    void append(const std::string &key);
    void append(const std::string &key, const std::string &value);
    void append(const CommandLine &other);

    bool contains(const std::string &key) const;
    bool has_value(const std::string &key) const;
    bool get_value(const std::string &key, std::string &value) const;

private:
    std::unordered_map<std::string, std::string> map {};
    std::unordered_set<std::string> set {};
};
} // namespace util

#endif /* C79D8ACB_84F0_4DDC_AAE8_86A47A6EF3D9 */
