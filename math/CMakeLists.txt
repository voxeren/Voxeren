add_library(math STATIC
    "${CMAKE_CURRENT_LIST_DIR}/matrix4x4.cc"
    "${CMAKE_CURRENT_LIST_DIR}/vector2.cc"
    "${CMAKE_CURRENT_LIST_DIR}/vector3.cc")
target_include_directories(math PUBLIC "${CMAKE_SOURCE_DIR}")

if(WIN32)
    target_compile_definitions(math PUBLIC NOMINMAX)
    target_compile_definitions(math PUBLIC _USE_MATH_DEFINES)
endif()
