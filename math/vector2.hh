/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 00:43:27
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef D06ED68E_697C_422A_9EEA_C3EBE20364CF
#define D06ED68E_697C_422A_9EEA_C3EBE20364CF
#include <math/packed.hh>

namespace math
{
class Vector2 final {
public:
    constexpr Vector2() = default;
    constexpr Vector2(const float value);
    constexpr Vector2(const float xvalue, const float yvalue);

    template<typename packed_type>
    constexpr Vector2(const math::Packed<packed_type, 2> &pack);

    float length() const;
    float length_sqr() const;
    Vector2 normalized() const;

    float normalize_insitu();

    float distance(const Vector2 &vec) const;
    float distance_sqr(const Vector2 &vec) const;
    float dot(const Vector2 &vec) const;
    float angle(const Vector2 &vec) const;

    constexpr Vector2 operator+(const Vector2 &vec) const;
    constexpr Vector2 operator-(const Vector2 &vec) const;
    constexpr Vector2 operator+(const float scalar) const;
    constexpr Vector2 operator-(const float scalar) const;
    constexpr Vector2 operator*(const float scalar) const;
    constexpr Vector2 operator/(const float scalar) const;

    constexpr Vector2 &operator+=(const Vector2 &vec);
    constexpr Vector2 &operator-=(const Vector2 &vec);
    constexpr Vector2 &operator+=(const float scalar);
    constexpr Vector2 &operator-=(const float scalar);
    constexpr Vector2 &operator*=(const float scalar);
    constexpr Vector2 &operator/=(const float scalar);

    template<typename packed_type>
    constexpr void to_packed(math::Packed<packed_type, 2> &pack) const;

public:
    float x {0.0f};
    float y {0.0f};
};
} // namespace math


inline constexpr math::Vector2::Vector2(float value)
    : x{value}, y{value}
{

}

inline constexpr math::Vector2::Vector2(float xvalue, float yvalue)
    : x{xvalue}, y{yvalue}
{

}

template<typename packed_type>
inline constexpr math::Vector2::Vector2(const math::Packed<packed_type, 2> &pack)
    : x{static_cast<float>(pack[0])}, y{static_cast<float>(pack[1])}
{

}

inline constexpr math::Vector2 math::Vector2::operator+(const math::Vector2 &vec) const
{
    return math::Vector2{x + vec.x, y + vec.y};
}

inline constexpr math::Vector2 math::Vector2::operator-(const math::Vector2 &vec) const
{
    return math::Vector2{x - vec.x, y - vec.y};
}

inline constexpr math::Vector2 math::Vector2::operator+(const float scalar) const
{
    return math::Vector2{x + scalar, y + scalar};
}

inline constexpr math::Vector2 math::Vector2::operator-(const float scalar) const
{
    return math::Vector2{x - scalar, y - scalar};
}

inline constexpr math::Vector2 math::Vector2::operator*(const float scalar) const
{
    return math::Vector2{x * scalar, y * scalar};
}

inline constexpr math::Vector2 math::Vector2::operator/(const float scalar) const
{
    return math::Vector2{x / scalar, y / scalar};
}

inline constexpr math::Vector2 &math::Vector2::operator+=(const Vector2 &vec)
{
    x += vec.x;
    y += vec.y;
    return *this;
}

inline constexpr math::Vector2 &math::Vector2::operator-=(const Vector2 &vec)
{
    x -= vec.x;
    y -= vec.y;
    return *this;
}

inline constexpr math::Vector2 &math::Vector2::operator+=(const float scalar)
{
    x += scalar;
    y += scalar;
    return *this;
}

inline constexpr math::Vector2 &math::Vector2::operator-=(const float scalar)
{
    x -= scalar;
    y -= scalar;
    return *this;
}

inline constexpr math::Vector2 &math::Vector2::operator*=(const float scalar)
{
    x *= scalar;
    y *= scalar;
    return *this;
}

inline constexpr math::Vector2 &math::Vector2::operator/=(const float scalar)
{
    x /= scalar;
    y /= scalar;
    return *this;
}

template<typename packed_type>
inline constexpr void math::Vector2::to_packed(math::Packed<packed_type, 2> &pack) const
{
    pack[0] = static_cast<packed_type>(x);
    pack[1] = static_cast<packed_type>(y);
}

#endif /* D06ED68E_697C_422A_9EEA_C3EBE20364CF */
