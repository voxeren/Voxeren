/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 08:16:41
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef A97CA342_C2F5_4B62_B300_83F8A60289E8
#define A97CA342_C2F5_4B62_B300_83F8A60289E8
#include <math/vector4.hh>

namespace math
{
class Matrix4x4 final {
public:
    constexpr Matrix4x4() = default;
    constexpr Matrix4x4(const Vector4 &a, const Vector4 &b, const Vector4 &c, const Vector4 &d);

    template<typename packed_type>
    constexpr Matrix4x4(const Packed<packed_type, 16> &pack);

    constexpr float determinant() const;
    constexpr Matrix4x4 transposed() const;
    constexpr Matrix4x4 inverse() const;

    void translate_insitu(const Vector3 &offset);
    void rotate_insitu(const Vector3 &axis, const float angle);
    void scale_insitu(const Vector3 &factors);

    template<typename packed_type>
    constexpr void to_packed(Packed<packed_type, 16> &pack) const;

    constexpr Matrix4x4 operator*(const Matrix4x4 &m) const;
    constexpr Matrix4x4 operator*(const float scalar) const;
    constexpr Matrix4x4 operator/(const float scalar) const;
    constexpr Vector4 operator*(const Vector4 &vec) const;

    constexpr Matrix4x4 &operator*=(const Matrix4x4 &m);
    constexpr Matrix4x4 &operator*=(const float scalar);
    constexpr Matrix4x4 &operator/=(const float scalar);

public:
    Vector4 row_1 {1.0f, 0.0f, 0.0f, 0.0f};
    Vector4 row_2 {0.0f, 1.0f, 0.0f, 0.0f};
    Vector4 row_3 {0.0f, 0.0f, 1.0f, 0.0f};
    Vector4 row_4 {0.0f, 0.0f, 0.0f, 1.0f};

public:
    static Matrix4x4 translate(const Vector3 &offset);
    static Matrix4x4 rotate(const Vector3 &axis, const float angle);
    static Matrix4x4 scale(const Vector3 &factors);

public:
    static Matrix4x4 look_at(const Vector3 &eye, const Vector3 &center, const Vector3 &up);
    static Matrix4x4 ortho(const float xmin, const float xmax, const float ymin, const float ymax, const float zmin, const float zmax);
    static Matrix4x4 persp(const float fov, const float aspect, const float zmin, const float zmax);
};
} // namespace math

namespace math::detail
{
inline constexpr float det3x3(const math::Vector3 &row_1, const math::Vector3 &row_2, const math::Vector3 &row_3)
{
    const float aek = row_1.x * row_2.y * row_3.z;
    const float bfi = row_1.y * row_2.z * row_3.x;
    const float cdj = row_1.z * row_2.x * row_3.y;
    const float iec = row_3.x * row_2.y * row_1.z;
    const float jfa = row_3.y * row_2.z * row_1.x;
    const float kdb = row_3.z * row_2.x * row_1.y;
    return aek + bfi + cdj - iec - jfa - kdb;
}

inline constexpr math::Vector4 get_column_1(const math::Matrix4x4 *mtx)
{
    return math::Vector4{mtx->row_1.x, mtx->row_2.x, mtx->row_3.x, mtx->row_4.x};
}

inline constexpr math::Vector4 get_column_2(const math::Matrix4x4 *mtx)
{    
    return math::Vector4{mtx->row_1.y, mtx->row_2.y, mtx->row_3.y, mtx->row_4.y};
}

inline constexpr math::Vector4 get_column_3(const math::Matrix4x4 *mtx)
{
    return math::Vector4{mtx->row_1.z, mtx->row_2.z, mtx->row_3.z, mtx->row_4.z};
}

inline constexpr math::Vector4 get_column_4(const math::Matrix4x4 *mtx)
{
    return math::Vector4{mtx->row_1.w, mtx->row_2.w, mtx->row_3.w, mtx->row_4.w};
}

inline constexpr void set_column_1(math::Matrix4x4 *mtx, const math::Vector4 &vec)
{
    mtx->row_1.x = vec.x;
    mtx->row_2.x = vec.y;
    mtx->row_3.x = vec.z;
    mtx->row_4.x = vec.w;
}

inline constexpr void set_column_2(math::Matrix4x4 *mtx, const math::Vector4 &vec)
{
    mtx->row_1.y = vec.x;
    mtx->row_2.y = vec.y;
    mtx->row_3.y = vec.z;
    mtx->row_4.y = vec.w;
}

inline constexpr void set_column_3(math::Matrix4x4 *mtx, const math::Vector4 &vec)
{
    mtx->row_1.z = vec.x;
    mtx->row_2.z = vec.y;
    mtx->row_3.z = vec.z;
    mtx->row_4.z = vec.w;
}

inline constexpr void set_column_4(math::Matrix4x4 *mtx, const math::Vector4 &vec)
{
    mtx->row_1.w = vec.x;
    mtx->row_2.w = vec.y;
    mtx->row_3.w = vec.z;
    mtx->row_4.w = vec.w;
}
} // namespace math::detail

inline constexpr math::Matrix4x4::Matrix4x4(const math::Vector4 &a, const math::Vector4 &b, const math::Vector4 &c, const math::Vector4 &d)
    : row_1{a}, row_2{b}, row_3{c}, row_4{d}
{

}

template<typename packed_type>
inline constexpr math::Matrix4x4::Matrix4x4(const math::Packed<packed_type, 16> &pack)
{
    row_1.x = static_cast<float>(pack[0x00]);
    row_1.y = static_cast<float>(pack[0x01]);
    row_1.z = static_cast<float>(pack[0x02]);
    row_1.w = static_cast<float>(pack[0x03]);

    row_2.x = static_cast<float>(pack[0x04]);
    row_2.y = static_cast<float>(pack[0x05]);
    row_2.z = static_cast<float>(pack[0x06]);
    row_2.w = static_cast<float>(pack[0x07]);

    row_3.x = static_cast<float>(pack[0x08]);
    row_3.y = static_cast<float>(pack[0x09]);
    row_3.z = static_cast<float>(pack[0x0A]);
    row_3.w = static_cast<float>(pack[0x0B]);

    row_4.x = static_cast<float>(pack[0x0C]);
    row_4.y = static_cast<float>(pack[0x0D]);
    row_4.z = static_cast<float>(pack[0x0E]);
    row_4.w = static_cast<float>(pack[0x0F]);
}

inline constexpr float math::Matrix4x4::determinant() const
{
    const float mx11 = row_1.x * math::detail::det3x3({row_2.y, row_2.z, row_2.w}, {row_3.y, row_3.z, row_3.w}, {row_4.y, row_4.z, row_4.w});
    const float mx21 = row_2.x * math::detail::det3x3({row_1.y, row_1.z, row_1.w}, {row_3.y, row_3.z, row_3.w}, {row_4.y, row_4.z, row_4.w});
    const float mx31 = row_3.x * math::detail::det3x3({row_1.y, row_1.z, row_1.w}, {row_2.y, row_2.z, row_2.w}, {row_4.y, row_4.z, row_4.w});
    const float mx41 = row_4.x * math::detail::det3x3({row_1.y, row_1.z, row_1.w}, {row_2.y, row_2.z, row_2.w}, {row_3.y, row_3.z, row_3.w});
    return mx11 - mx21 + mx31 - mx41;
}

inline constexpr math::Matrix4x4 math::Matrix4x4::transposed() const
{
    const math::Vector4 a = row_1;
    const math::Vector4 b = row_2;
    const math::Vector4 c = row_3;
    const math::Vector4 d = row_4;

    return math::Matrix4x4 {
        math::Vector4{a.x, b.x, c.x, d.x},
        math::Vector4{a.y, b.y, c.y, d.y},
        math::Vector4{a.z, b.z, c.z, d.z},
        math::Vector4{a.w, b.w, c.w, d.w},
    };
}

inline constexpr math::Matrix4x4 math::Matrix4x4::inverse() const
{
    const math::Matrix4x4 minor_transposed = {
        math::Vector4 {
            +math::detail::det3x3({row_2.y, row_2.z, row_2.w}, {row_3.y, row_3.z, row_3.w}, {row_4.y, row_4.z, row_4.w}),
            -math::detail::det3x3({row_1.y, row_1.z, row_1.w}, {row_3.y, row_3.z, row_3.w}, {row_4.y, row_4.z, row_4.w}),
            +math::detail::det3x3({row_1.y, row_1.z, row_1.w}, {row_2.y, row_2.z, row_2.w}, {row_4.y, row_4.z, row_4.w}),
            -math::detail::det3x3({row_1.y, row_1.z, row_1.w}, {row_2.y, row_2.z, row_2.w}, {row_3.y, row_3.z, row_3.w}),
        },
        math::Vector4 {
            -math::detail::det3x3({row_2.x, row_2.z, row_2.w}, {row_3.x, row_3.z, row_3.w}, {row_4.x, row_4.z, row_4.w}),
            +math::detail::det3x3({row_1.x, row_1.z, row_1.w}, {row_3.x, row_3.z, row_3.w}, {row_4.x, row_4.z, row_4.w}),
            -math::detail::det3x3({row_1.x, row_1.z, row_1.w}, {row_2.x, row_2.z, row_2.w}, {row_4.x, row_4.z, row_4.w}),
            +math::detail::det3x3({row_1.x, row_1.z, row_1.w}, {row_2.x, row_2.z, row_2.w}, {row_3.x, row_3.z, row_3.w}),
        },
        math::Vector4 {
            +math::detail::det3x3({row_2.x, row_2.y, row_2.w}, {row_3.x, row_3.y, row_3.w}, {row_4.x, row_4.y, row_4.w}),
            -math::detail::det3x3({row_1.x, row_1.y, row_1.w}, {row_3.x, row_3.y, row_3.w}, {row_4.x, row_4.y, row_4.w}),
            +math::detail::det3x3({row_1.x, row_1.y, row_1.w}, {row_2.x, row_2.y, row_2.w}, {row_4.x, row_4.y, row_4.w}),
            -math::detail::det3x3({row_1.x, row_1.y, row_1.w}, {row_2.x, row_2.y, row_2.w}, {row_3.x, row_3.y, row_3.w}),
        },
        math::Vector4 {
            -math::detail::det3x3({row_2.x, row_2.y, row_2.z}, {row_3.x, row_3.y, row_3.z}, {row_4.x, row_4.y, row_4.z}),
            +math::detail::det3x3({row_1.x, row_1.y, row_1.z}, {row_3.x, row_3.y, row_3.z}, {row_4.x, row_4.y, row_4.z}),
            -math::detail::det3x3({row_1.x, row_1.y, row_1.z}, {row_2.x, row_2.y, row_2.z}, {row_4.x, row_4.y, row_4.z}),
            +math::detail::det3x3({row_1.x, row_1.y, row_1.z}, {row_2.x, row_2.y, row_2.z}, {row_3.x, row_3.y, row_3.z}),
        }
    };

    // We have already calculated submatrices determinants so why
    // not just use them instead of calling this->determinant()
    const float mx11 = row_1.x * minor_transposed.row_1.x;
    const float mx21 = row_2.x * minor_transposed.row_1.y;
    const float mx31 = row_3.x * minor_transposed.row_1.z;
    const float mx41 = row_4.x * minor_transposed.row_1.w;
    const float det = mx11 + mx21 + mx31 + mx41;

    return math::Matrix4x4 {
        minor_transposed.row_1 / det,
        minor_transposed.row_2 / det,
        minor_transposed.row_3 / det,
        minor_transposed.row_4 / det,
    };
}

template<typename packed_type>
inline constexpr void math::Matrix4x4::to_packed(math::Packed<packed_type, 16> &pack) const
{
    pack[0x00] = static_cast<packed_type>(row_1.x);
    pack[0x01] = static_cast<packed_type>(row_1.y);
    pack[0x02] = static_cast<packed_type>(row_1.z);
    pack[0x03] = static_cast<packed_type>(row_1.w);

    pack[0x04] = static_cast<packed_type>(row_2.x);
    pack[0x05] = static_cast<packed_type>(row_2.y);
    pack[0x06] = static_cast<packed_type>(row_2.z);
    pack[0x07] = static_cast<packed_type>(row_2.w);

    pack[0x08] = static_cast<packed_type>(row_3.x);
    pack[0x09] = static_cast<packed_type>(row_3.y);
    pack[0x0A] = static_cast<packed_type>(row_3.z);
    pack[0x0B] = static_cast<packed_type>(row_3.w);

    pack[0x0C] = static_cast<packed_type>(row_4.x);
    pack[0x0D] = static_cast<packed_type>(row_4.y);
    pack[0x0E] = static_cast<packed_type>(row_4.z);
    pack[0x0F] = static_cast<packed_type>(row_4.w);
}

inline constexpr math::Matrix4x4 math::Matrix4x4::operator*(const math::Matrix4x4 &m) const
{
    return math::Matrix4x4 {
        math::Vector4 {
            row_1.x * m.row_1.x + row_1.y * m.row_2.x + row_1.z * m.row_3.x + row_1.w * m.row_4.x,
            row_1.x * m.row_1.y + row_1.y * m.row_2.y + row_1.z * m.row_3.y + row_1.w * m.row_4.y,
            row_1.x * m.row_1.z + row_1.y * m.row_2.z + row_1.z * m.row_3.z + row_1.w * m.row_4.z,
            row_1.x * m.row_1.w + row_1.y * m.row_2.w + row_1.z * m.row_3.w + row_1.w * m.row_4.w,
        },
        math::Vector4 {
            row_2.x * m.row_1.x + row_2.y * m.row_2.x + row_2.z * m.row_3.x + row_2.w * m.row_4.x,
            row_2.x * m.row_1.y + row_2.y * m.row_2.y + row_2.z * m.row_3.y + row_2.w * m.row_4.y,
            row_2.x * m.row_1.z + row_2.y * m.row_2.z + row_2.z * m.row_3.z + row_2.w * m.row_4.z,
            row_2.x * m.row_1.w + row_2.y * m.row_2.w + row_2.z * m.row_3.w + row_2.w * m.row_4.w,
        },
        math::Vector4 {
            row_3.x * m.row_1.x + row_3.y * m.row_2.x + row_3.z * m.row_3.x + row_3.w * m.row_4.x,
            row_3.x * m.row_1.y + row_3.y * m.row_2.y + row_3.z * m.row_3.y + row_3.w * m.row_4.y,
            row_3.x * m.row_1.z + row_3.y * m.row_2.z + row_3.z * m.row_3.z + row_3.w * m.row_4.z,
            row_3.x * m.row_1.w + row_3.y * m.row_2.w + row_3.z * m.row_3.w + row_3.w * m.row_4.w,
        },
        math::Vector4 {
            row_4.x * m.row_1.x + row_4.y * m.row_2.x + row_4.z * m.row_3.x + row_4.w * m.row_4.x,
            row_4.x * m.row_1.y + row_4.y * m.row_2.y + row_4.z * m.row_3.y + row_4.w * m.row_4.y,
            row_4.x * m.row_1.z + row_4.y * m.row_2.z + row_4.z * m.row_3.z + row_4.w * m.row_4.z,
            row_4.x * m.row_1.w + row_4.y * m.row_2.w + row_4.z * m.row_3.w + row_4.w * m.row_4.w,
        }
    };
}

inline constexpr math::Matrix4x4 math::Matrix4x4::operator*(const float scalar) const
{
    return math::Matrix4x4{row_1 * scalar, row_2 * scalar, row_3 * scalar, row_4 * scalar};
}

inline constexpr math::Matrix4x4 math::Matrix4x4::operator/(const float scalar) const
{
    return math::Matrix4x4{row_1 / scalar, row_2 / scalar, row_3 / scalar, row_4 / scalar};
}

inline constexpr math::Vector4 math::Matrix4x4::operator*(const math::Vector4 &vec) const
{
    return math::Vector4 {
        row_1.x * vec.x + row_1.y * vec.y + row_1.z * vec.z + row_1.w * vec.w,
        row_2.x * vec.x + row_2.y * vec.y + row_2.z * vec.z + row_2.w * vec.w,
        row_3.x * vec.x + row_3.y * vec.y + row_3.z * vec.z + row_3.w * vec.w,
        row_4.x * vec.x + row_4.y * vec.y + row_4.z * vec.z + row_4.w * vec.w,
    };
}

inline constexpr math::Matrix4x4 &math::Matrix4x4::operator*=(const math::Matrix4x4 &m)
{
    const math::Matrix4x4 result = {
        math::Vector4 {
            row_1.x * m.row_1.x + row_1.y * m.row_2.x + row_1.z * m.row_3.x + row_1.w * m.row_4.x,
            row_1.x * m.row_1.y + row_1.y * m.row_2.y + row_1.z * m.row_3.y + row_1.w * m.row_4.y,
            row_1.x * m.row_1.z + row_1.y * m.row_2.z + row_1.z * m.row_3.z + row_1.w * m.row_4.z,
            row_1.x * m.row_1.w + row_1.y * m.row_2.w + row_1.z * m.row_3.w + row_1.w * m.row_4.w,
        },
        math::Vector4 {
            row_2.x * m.row_1.x + row_2.y * m.row_2.x + row_2.z * m.row_3.x + row_2.w * m.row_4.x,
            row_2.x * m.row_1.y + row_2.y * m.row_2.y + row_2.z * m.row_3.y + row_2.w * m.row_4.y,
            row_2.x * m.row_1.z + row_2.y * m.row_2.z + row_2.z * m.row_3.z + row_2.w * m.row_4.z,
            row_2.x * m.row_1.w + row_2.y * m.row_2.w + row_2.z * m.row_3.w + row_2.w * m.row_4.w,
        },
        math::Vector4 {
            row_3.x * m.row_1.x + row_3.y * m.row_2.x + row_3.z * m.row_3.x + row_3.w * m.row_4.x,
            row_3.x * m.row_1.y + row_3.y * m.row_2.y + row_3.z * m.row_3.y + row_3.w * m.row_4.y,
            row_3.x * m.row_1.z + row_3.y * m.row_2.z + row_3.z * m.row_3.z + row_3.w * m.row_4.z,
            row_3.x * m.row_1.w + row_3.y * m.row_2.w + row_3.z * m.row_3.w + row_3.w * m.row_4.w,
        },
        math::Vector4 {
            row_4.x * m.row_1.x + row_4.y * m.row_2.x + row_4.z * m.row_3.x + row_4.w * m.row_4.x,
            row_4.x * m.row_1.y + row_4.y * m.row_2.y + row_4.z * m.row_3.y + row_4.w * m.row_4.y,
            row_4.x * m.row_1.z + row_4.y * m.row_2.z + row_4.z * m.row_3.z + row_4.w * m.row_4.z,
            row_4.x * m.row_1.w + row_4.y * m.row_2.w + row_4.z * m.row_3.w + row_4.w * m.row_4.w,
        }
    };

    row_1 = result.row_1;
    row_2 = result.row_2;
    row_3 = result.row_3;
    row_4 = result.row_4;

    return *this;
}

inline constexpr math::Matrix4x4 &math::Matrix4x4::operator*=(const float scalar)
{
    row_1 *= scalar;
    row_2 *= scalar;
    row_3 *= scalar;
    row_4 *= scalar;
    return *this;
}

inline constexpr math::Matrix4x4 &math::Matrix4x4::operator/=(const float scalar)
{
    row_1 /= scalar;
    row_2 /= scalar;
    row_3 /= scalar;
    row_4 /= scalar;
    return *this;
}

#endif /* A97CA342_C2F5_4B62_B300_83F8A60289E8 */
