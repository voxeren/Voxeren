/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Sat Aug 05 2023 14:56:19
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef FF3A04DE_6476_4005_BA8B_44A1C7323264
#define FF3A04DE_6476_4005_BA8B_44A1C7323264
#include <math/vector3.hh>

namespace math
{
class Quaternion final {
public:
    constexpr Quaternion() = default;
    constexpr Quaternion(float angle, const math::Vector3 &axis);
    constexpr Quaternion(float wvalue, float xvalue, float yvalue, float zvalue);

    template<typename packed_type>
    constexpr Quaternion(const math::Packed<packed_type, 4> &pack);

    float length() const;
    float length_sqr() const;
    Quaternion normalized() const;
    Quaternion conjugate() const;
    Quaternion inverse() const;

    float normalize_insitu();
    float invert_insitu();

    Vector3 product(const Vector3 &vec) const;

    constexpr Quaternion operator+(const Quaternion &quat) const;
    constexpr Quaternion operator-(const Quaternion &quat) const;
    constexpr Quaternion operator*(const Quaternion &quat) const;
    constexpr Quaternion operator+(const float scalar) const;
    constexpr Quaternion operator-(const float scalar) const;
    constexpr Quaternion operator*(const float scalar) const;
    constexpr Quaternion operator/(const float scalar) const;

    constexpr Quaternion &operator+=(const Quaternion &vec);
    constexpr Quaternion &operator-=(const Quaternion &vec);
    constexpr Quaternion &operator*=(const Quaternion &vec);
    constexpr Quaternion &operator+=(const float scalar);
    constexpr Quaternion &operator-=(const float scalar);
    constexpr Quaternion &operator*=(const float scalar);
    constexpr Quaternion &operator/=(const float scalar);

    template<typename packed_type>
    constexpr void to_packed(math::Packed<packed_type, 4> &pack) const;

public:
    float w {0.0f};
    float x {0.0f};
    float y {0.0f};
    float z {0.0f};
};
} // namespace math

inline constexpr math::Quaternion::Quaternion(float angle, const math::Vector3 &axis)
{
    w = angle;
    x = axis.x;
    y = axis.y;
    z = axis.z;
}

inline constexpr math::Quaternion::Quaternion(float wvalue, float xvalue, float yvalue, float zvalue)
{
    w = wvalue;
    x = xvalue;
    y = yvalue;
    z = zvalue;
}

template<typename packed_type>
inline constexpr math::Quaternion::Quaternion(const math::Packed<packed_type, 4> &packed)
{
    w = static_cast<float>(packed[0]);
    x = static_cast<float>(packed[1]);
    y = static_cast<float>(packed[2]);
    z = static_cast<float>(packed[3]);
}


#endif /* FF3A04DE_6476_4005_BA8B_44A1C7323264 */
