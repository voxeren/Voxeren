/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 01:27:59
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <math/vector3.hh>
#include <math.h>

float math::Vector3::length() const
{
    return sqrtf(x * x + y * y + z * z);
}

float math::Vector3::length_sqr() const
{
    return x * x + y * y + z * z;
}

math::Vector3 math::Vector3::normalized() const
{
    const float norm = length();
    return Vector3{x / norm, y / norm, z / norm};
}

float math::Vector3::normalize_insitu()
{
    const float norm = length();
    x /= norm;
    y /= norm;
    z /= norm;
    return norm;
}

float math::Vector3::distance(const Vector3 &vec) const
{
    const float dx = x - vec.x;
    const float dy = y - vec.y;
    const float dz = z - vec.z;
    return sqrtf(dx * dx + dy * dy + dz * dz);
}

float math::Vector3::distance_sqr(const Vector3 &vec) const
{
    const float dx = x - vec.x;
    const float dy = y - vec.y;
    const float dz = z - vec.z;
    return dx * dx + dy * dy + dz * dz;
}

float math::Vector3::dot(const Vector3 &vec) const
{
    return x * vec.x + y * vec.y + z * vec.z;
}

math::Vector3 math::Vector3::cross(const Vector3 &vec) const
{
    return math::Vector3{y * vec.z + z * vec.y, z * vec.x + x * vec.z, x * vec.y + y * vec.x};
}

float math::Vector3::angle(const Vector3 &vec) const
{
    const float product = dot(vec);
    const float norm_a = length_sqr();
    const float norm_b = vec.length_sqr();
    return product / sqrtf(norm_a * norm_b);
}
