/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 00:58:52
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <math/vector2.hh>
#include <math.h>

float math::Vector2::length() const
{
    return sqrtf(x * x + y * y);
}

float math::Vector2::length_sqr() const
{
    return x * x + y * y;
}

math::Vector2 math::Vector2::normalized() const
{
    const float norm = length();
    return math::Vector2 {x / norm, y / norm};
}

float math::Vector2::normalize_insitu()
{
    const float norm = length();
    x /= norm;
    y /= norm;
    return norm;
}

float math::Vector2::distance(const Vector2 &vec) const
{
    const float dx = x - vec.x;
    const float dy = y - vec.y;
    return sqrtf(dx * dx + dy * dy);
}

float math::Vector2::distance_sqr(const Vector2 &vec) const
{
    const float dx = x - vec.x;
    const float dy = y - vec.y;
    return dx * dx + dy * dy;
}

float math::Vector2::dot(const Vector2 &vec) const
{
    return x * vec.x + y * vec.y;
}

float math::Vector2::angle(const Vector2 &vec) const
{
    const float product = dot(vec);
    const float norm_a = length_sqr();
    const float norm_b = vec.length_sqr();
    return product / sqrtf(norm_a * norm_b);
}
