/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 08:11:16
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef E27B66AF_32BE_4536_89DE_3EF1CAE5C6B9
#define E27B66AF_32BE_4536_89DE_3EF1CAE5C6B9
#include <math/vector2.hh>
#include <math/vector3.hh>

namespace math
{
class Vector4 final {
public:
    constexpr Vector4() = default;
    constexpr Vector4(const float value);
    constexpr Vector4(const float xvalue, const float yvalue, const float zvalue, const float wvalue);
    constexpr Vector4(const Vector2 &left, const Vector2 &right);
    constexpr Vector4(const Vector3 &vec, const float wvalue);

    template<typename packed_type>
    constexpr Vector4(const Packed<packed_type, 4> &pack);

    constexpr Vector4 operator+(const Vector4 &vec) const;
    constexpr Vector4 operator-(const Vector4 &vec) const;
    constexpr Vector4 operator+(const float scalar) const;
    constexpr Vector4 operator-(const float scalar) const;
    constexpr Vector4 operator*(const float scalar) const;
    constexpr Vector4 operator/(const float scalar) const;

    constexpr Vector4 &operator+=(const Vector4 &vec);
    constexpr Vector4 &operator-=(const Vector4 &vec);
    constexpr Vector4 &operator+=(const float scalar);
    constexpr Vector4 &operator-=(const float scalar);
    constexpr Vector4 &operator*=(const float scalar);
    constexpr Vector4 &operator/=(const float scalar);

    template<typename packed_type>
    constexpr void to_packed(Packed<packed_type, 4> &pack) const;

public:
    float x {0.0f};
    float y {0.0f};
    float z {0.0f};
    float w {0.0f};
};
} // namespace math

inline constexpr math::Vector4::Vector4(const float value)
    : x{value}, y{value}, z{value}
{

}

inline constexpr math::Vector4::Vector4(const float xvalue, const float yvalue, const float zvalue, const float wvalue)
    : x{xvalue}, y{yvalue}, z{zvalue}, w{wvalue}
{

}

inline constexpr math::Vector4::Vector4(const math::Vector2 &left, const math::Vector2 &right)
    : x{left.x}, y{left.y}, z{right.x}, w{right.y}
{

}

inline constexpr math::Vector4::Vector4(const math::Vector3 &vec, const float wvalue)
    : x{vec.x}, y{vec.y}, z{vec.z}, w{wvalue}
{

}

template<typename packed_type>
inline constexpr math::Vector4::Vector4(const math::Packed<packed_type, 4> &pack)
    : x{static_cast<float>(pack[0])}, y{static_cast<float>(pack[1])}, z{static_cast<float>(pack[2])}, w{static_cast<float>(pack[3])}
{

}

inline constexpr math::Vector4 math::Vector4::operator+(const math::Vector4 &vec) const
{
    return math::Vector4{x + vec.x, y + vec.y, z + vec.z, w + vec.w};
}

inline constexpr math::Vector4 math::Vector4::operator-(const math::Vector4 &vec) const
{
    return math::Vector4{x - vec.x, y - vec.y, z - vec.z, w - vec.w};
}

inline constexpr math::Vector4 math::Vector4::operator+(const float scalar) const
{
    return math::Vector4{x + scalar, y + scalar, z + scalar, w + scalar};
}

inline constexpr math::Vector4 math::Vector4::operator-(const float scalar) const
{
    return math::Vector4{x - scalar, y - scalar, z - scalar, w - scalar};
}

inline constexpr math::Vector4 math::Vector4::operator*(const float scalar) const
{
    return math::Vector4{x * scalar, y * scalar, z * scalar, w * scalar};
}

inline constexpr math::Vector4 math::Vector4::operator/(const float scalar) const
{
    return math::Vector4{x / scalar, y / scalar, z / scalar, w / scalar};
}

inline constexpr math::Vector4 &math::Vector4::operator+=(const Vector4 &vec)
{
    x += vec.x;
    y += vec.y;
    z += vec.z;
    w += vec.w;
    return *this;
}

inline constexpr math::Vector4 &math::Vector4::operator-=(const Vector4 &vec)
{
    x -= vec.x;
    y -= vec.y;
    z -= vec.z;
    w -= vec.w;
    return *this;
}

inline constexpr math::Vector4 &math::Vector4::operator+=(const float scalar)
{
    x += scalar;
    y += scalar;
    z += scalar;
    w += scalar;
    return *this;
}

inline constexpr math::Vector4 &math::Vector4::operator-=(const float scalar)
{
    x -= scalar;
    y -= scalar;
    z -= scalar;
    w -= scalar;
    return *this;
}

inline constexpr math::Vector4 &math::Vector4::operator*=(const float scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;
    w *= scalar;
    return *this;
}

inline constexpr math::Vector4 &math::Vector4::operator/=(const float scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;
    w /= scalar;
    return *this;
}

template<typename packed_type>
inline constexpr void math::Vector4::to_packed(math::Packed<packed_type, 4> &pack) const
{
    pack[0] = static_cast<packed_type>(x);
    pack[1] = static_cast<packed_type>(y);
    pack[2] = static_cast<packed_type>(z);
    pack[3] = static_cast<packed_type>(w);
}

#endif /* E27B66AF_32BE_4536_89DE_3EF1CAE5C6B9 */
