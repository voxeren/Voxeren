/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 08:28:34
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <math/matrix4x4.hh>
#include <math.h>

void math::Matrix4x4::translate_insitu(const math::Vector3 &offset)
{
    const math::Vector4 col_1 = math::detail::get_column_1(this);
    const math::Vector4 col_2 = math::detail::get_column_2(this);
    const math::Vector4 col_3 = math::detail::get_column_3(this);
    const math::Vector4 col_4 = math::detail::get_column_4(this);
    math::detail::set_column_4(this, col_1 * offset.x + col_2 * offset.y + col_3 * offset.z + col_4);
}

void math::Matrix4x4::rotate_insitu(const math::Vector3 &axis, const float angle)
{
    const float cv = cosf(angle);
    const float sv = sinf(angle);
    const math::Vector3 a = axis.normalized();
    const math::Vector3 t = a * (1.0f - cv);

    const math::Vector3 rcol_1 = math::Vector3{t.x * a.x + cv, t.x * a.y + sv * a.z, t.x * a.z - sv * a.y};
    const math::Vector3 rcol_2 = math::Vector3{t.y * a.x - sv * a.z, t.y * a.y + cv, t.y * a.z + sv * a.x};
    const math::Vector3 rcol_3 = math::Vector3{t.z * a.x + sv * a.y, t.z * a.y - sv * a.x, t.z * a.z + cv};

    const math::Vector4 mcol_1 = math::detail::get_column_1(this);
    const math::Vector4 mcol_2 = math::detail::get_column_2(this);
    const math::Vector4 mcol_3 = math::detail::get_column_3(this);

    math::detail::set_column_1(this, mcol_1 * rcol_1.x + mcol_2 * rcol_1.y + mcol_3 * rcol_1.z);
    math::detail::set_column_2(this, mcol_1 * rcol_2.x + mcol_2 * rcol_2.y + mcol_3 * rcol_2.z);
    math::detail::set_column_3(this, mcol_1 * rcol_3.x + mcol_2 * rcol_3.y + mcol_3 * rcol_3.z);
}

void math::Matrix4x4::scale_insitu(const math::Vector3 &factors)
{
    const math::Vector4 col_1 = math::detail::get_column_1(this);
    const math::Vector4 col_2 = math::detail::get_column_2(this);
    const math::Vector4 col_3 = math::detail::get_column_3(this);

    math::detail::set_column_1(this, col_1 * factors.x);
    math::detail::set_column_2(this, col_2 * factors.y);
    math::detail::set_column_3(this, col_3 * factors.z);
}

math::Matrix4x4 math::Matrix4x4::translate(const math::Vector3 &offset)
{
    math::Matrix4x4 mtx = {};
    math::detail::set_column_4(&mtx, math::Vector4{offset, 1.0f});
    return mtx;
}

math::Matrix4x4 math::Matrix4x4::rotate(const math::Vector3 &axis, const float angle)
{
    const float cv = cosf(angle);
    const float sv = sinf(angle);
    const float cf = 1.0f - cv;
    const math::Vector3 a = axis.normalized();

    math::Matrix4x4 mtx = {};
    math::detail::set_column_1(&mtx, math::Vector4{cf * a.x * a.x + cv, cf * a.x * a.y + sv * a.z, cf * a.x * a.z - sv * a.y, 0.0});
    math::detail::set_column_2(&mtx, math::Vector4{cf * a.y * a.x - sv * a.z, cf * a.y * a.y + cv, cf * a.y * a.z + sv * a.x, 0.0});
    math::detail::set_column_3(&mtx, math::Vector4{cf * a.x * a.x + sv * a.y, cf * a.z * a.y - sv * a.x, cf * a.z * a.z + cv, 0.0});
    return mtx;
}

math::Matrix4x4 math::Matrix4x4::scale(const math::Vector3 &factors)
{
    math::Matrix4x4 mtx = {};
    mtx.row_1.x = factors.x;
    mtx.row_2.y = factors.y;
    mtx.row_3.z = factors.z;
    return mtx;
}

math::Matrix4x4 math::Matrix4x4::look_at(const math::Vector3 &eye, const math::Vector3 &center, const Vector3 &up)
{
    const math::Vector3 f = math::Vector3{center - eye}.normalized();
    const math::Vector3 s = f.cross(up).normalized();
    const math::Vector3 u = s.cross(f);

    math::Matrix4x4 mtx = {};
    mtx.row_1 = math::Vector4{s.x, s.y, s.z, -s.dot(eye)};
    mtx.row_2 = math::Vector4{u.x, u.y, u.z, -u.dot(eye)};
    mtx.row_3 = math::Vector4{-f.x, -f.y, -f.z, f.dot(eye)};
    return mtx;
}

math::Matrix4x4 math::Matrix4x4::ortho(const float xmin, const float xmax, const float ymin, const float ymax, const float zmin, const float zmax)
{
    math::Matrix4x4 mtx = {};
    mtx.row_1.x = 2.0f / (xmax - xmin);
    mtx.row_2.y = 2.0f / (ymax - ymin);
    mtx.row_3.z = -2.0f / (zmax - zmin);
    mtx.row_1.w = -1.0f * (xmax + xmin) / (xmax - xmin);
    mtx.row_2.w = -1.0f * (ymax + ymin) / (ymax - ymin);
    mtx.row_3.w = -1.0f * (zmax + zmin) / (zmax - zmin);
    return mtx;
}

math::Matrix4x4 math::Matrix4x4::persp(const float fov, const float aspect, const float zmin, const float zmax)
{
    const float tfov = tanf(0.5f * fov);

    math::Matrix4x4 mtx = {};
    mtx.row_1.x = 1.0f / (tfov * aspect);
    mtx.row_2.y = 1.0f / (tfov);
    mtx.row_3.z = zmax / (zmax - zmin);
    mtx.row_4.z = 1.0f;
    mtx.row_3.w = -1.0f * (zmax * zmin) / (zmax - zmin);
    return mtx;
}
