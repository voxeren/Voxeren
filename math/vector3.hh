/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 01:07:52
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef FCD08F02_A971_42DD_A0FA_792D4A5A1416
#define FCD08F02_A971_42DD_A0FA_792D4A5A1416
#include <math/packed.hh>

namespace math
{
class Vector3 final {
public:
    constexpr Vector3() = default;
    constexpr Vector3(const float value);
    constexpr Vector3(const float xvalue, const float yvalue, const float zvalue);

    template<typename packed_type>
    constexpr Vector3(const math::Packed<packed_type, 3> &pack);

    float length() const;
    float length_sqr() const;
    Vector3 normalized() const;

    float normalize_insitu();

    float distance(const Vector3 &vec) const;
    float distance_sqr(const Vector3 &vec) const;
    float dot(const Vector3 &vec) const;
    float angle(const Vector3 &vec) const;
    Vector3 cross(const Vector3 &vec) const;

    constexpr Vector3 operator+(const Vector3 &vec) const;
    constexpr Vector3 operator-(const Vector3 &vec) const;
    constexpr Vector3 operator+(const float scalar) const;
    constexpr Vector3 operator-(const float scalar) const;
    constexpr Vector3 operator*(const float scalar) const;
    constexpr Vector3 operator/(const float scalar) const;

    constexpr Vector3 &operator+=(const Vector3 &vec);
    constexpr Vector3 &operator-=(const Vector3 &vec);
    constexpr Vector3 &operator+=(const float scalar);
    constexpr Vector3 &operator-=(const float scalar);
    constexpr Vector3 &operator*=(const float scalar);
    constexpr Vector3 &operator/=(const float scalar);

    template<typename packed_type>
    constexpr void to_packed(math::Packed<packed_type, 3> &pack) const;

public:
    float x {0.0f};
    float y {0.0f};
    float z {0.0f};
};
} // namespace math

inline constexpr math::Vector3::Vector3(float value)
    : x{value}, y{value}, z{value}
{

}

inline constexpr math::Vector3::Vector3(float xvalue, float yvalue, float zvalue)
    : x{xvalue}, y{yvalue}, z{zvalue}
{

}

template<typename packed_type>
inline constexpr math::Vector3::Vector3(const math::Packed<packed_type, 3> &pack)
    : x{static_cast<float>(pack[0])}, y{static_cast<float>(pack[1])}, z{static_cast<float>(pack[2])}
{

}

inline constexpr math::Vector3 math::Vector3::operator+(const math::Vector3 &vec) const
{
    return math::Vector3{x + vec.x, y + vec.y, z + vec.z};
}

inline constexpr math::Vector3 math::Vector3::operator-(const math::Vector3 &vec) const
{
    return math::Vector3{x - vec.x, y - vec.y, z - vec.z};
}

inline constexpr math::Vector3 math::Vector3::operator+(const float scalar) const
{
    return math::Vector3{x + scalar, y + scalar, z + scalar};
}

inline constexpr math::Vector3 math::Vector3::operator-(const float scalar) const
{
    return math::Vector3{x - scalar, y - scalar, z - scalar};
}

inline constexpr math::Vector3 math::Vector3::operator*(const float scalar) const
{
    return math::Vector3{x * scalar, y * scalar, z * scalar};
}

inline constexpr math::Vector3 math::Vector3::operator/(const float scalar) const
{
    return math::Vector3{x / scalar, y / scalar, z / scalar};
}

inline constexpr math::Vector3 &math::Vector3::operator+=(const math::Vector3 &vec)
{
    x += vec.x;
    y += vec.y;
    z += vec.z;
    return *this;
}

inline constexpr math::Vector3 &math::Vector3::operator-=(const math::Vector3 &vec)
{
    x -= vec.x;
    y -= vec.y;
    z -= vec.z;
    return *this;
}

inline constexpr math::Vector3 &math::Vector3::operator+=(const float scalar)
{
    x += scalar;
    y += scalar;
    z += scalar;
    return *this;
}

inline constexpr math::Vector3 &math::Vector3::operator-=(const float scalar)
{
    x -= scalar;
    y -= scalar;
    z -= scalar;
    return *this;
}

inline constexpr math::Vector3 &math::Vector3::operator*=(const float scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;
    return *this;
}

inline constexpr math::Vector3 &math::Vector3::operator/=(const float scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;
    return *this;
}

template<typename packed_type>
inline constexpr void math::Vector3::to_packed(math::Packed<packed_type, 3> &pack) const
{
    pack[0] = static_cast<packed_type>(x);
    pack[1] = static_cast<packed_type>(y);
    pack[2] = static_cast<packed_type>(z);
}

#endif /* FCD08F02_A971_42DD_A0FA_792D4A5A1416 */
