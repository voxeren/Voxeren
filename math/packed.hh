/* SPDX-License-Identifier: MPL-2.0 */
/*
 * Copyright (c) 2023, Voxeren Contributors
 * Created: Fri Jul 14 2023 00:38:22
 * Author: Kirill GPRB <undetz@proton.me>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef DBB9F040_5756_431E_A485_806CDE7589AF
#define DBB9F040_5756_431E_A485_806CDE7589AF
#include <array>
#include <stddef.h>
#include <stdint.h>

namespace math
{
// Packed type is something that is guaranteed to
// be contiguous in memory without any alignment,
// allowing for easier data passthrough between
// CPU-side code and GPU-side shaders and whatnot.
// Common math objects (Vector2, Vector3, Vector4,
// Matrix4x4 and Quaternion) can be converted to
// a Packed type or initialized from one.
template<typename packed_type, size_t size>
using Packed = std::array<packed_type, size>;

using PackedFloat2 = Packed<float, 2>;
using PackedFloat3 = Packed<float, 3>;
using PackedFloat4 = Packed<float, 4>;
using PackedFloat16 = Packed<float, 16>;

using PackedUint2 = Packed<uint32_t, 2>;
using PackedUint3 = Packed<uint32_t, 3>;
using PackedUint4 = Packed<uint32_t, 4>;
} // namespace math

#endif /* DBB9F040_5756_431E_A485_806CDE7589AF */
